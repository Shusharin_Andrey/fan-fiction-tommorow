package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test12 : BaseAuth() {
    @Test
    fun projectInformation() {
        openInDrawerLayout(R.id.nav_settings)
        clickOnViewStringResource(R.string.info_about_app)
        TestCase.assertEquals(
            R.id.nav_info,
            MainActivity.navController.currentDestination!!.id
        )
    }
}