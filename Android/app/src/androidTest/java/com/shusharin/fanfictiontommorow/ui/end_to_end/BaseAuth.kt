package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.test.core.app.ActivityScenario
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import org.junit.After
import org.junit.Before
import java.lang.Thread.sleep

open class BaseAuth : Base() {

    @Before
    override fun create() {
        scenario = ActivityScenario.launch(MainActivity::class.java)
        login()
    }

    @After
    fun logoutAfter() {
        logout()
    }
}