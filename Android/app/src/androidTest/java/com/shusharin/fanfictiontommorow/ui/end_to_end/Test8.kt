package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.lifecycle.ViewModelProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genres
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusCreatedBooks
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.book.BookViewModel
import com.shusharin.fanfictiontomorrow.ui.create_and_edit_book.CreateAndEditFragment
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewModelFactory
import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
class Test8 : BaseAuth() {
    @Test
    fun creatingBook() {
        clickOnView(R.id.nav_my_profile)
        clickOnView(R.id.create_new_works)
        val nameBook = "Название"
        setText(R.id.name_book, nameBook)
        val annotate = "Аннотация книги"
        setText(R.id.annotate, annotate)
        clickOnView(R.id.genre)
        val genres = Genres.SCIENCE_FICTION
        clickOnTextInViewInDialog(genres.nameG)
        clickOnTextInViewInDialog(R.string.dialog_ok)
        clickOnView(R.id.status_create_book)
        clickOnTextInViewInDialog(StatusCreatedBooks.IN_PROCESS.nameSCB)
        clickOnTextInViewInDialog(R.string.dialog_ok)
        clickOnView(R.id.add_chapter)
        CreateAndEditFragment.edit.text.append("Название")
        clickOnTextInViewInDialog(R.string.dialog_add)
        clickOnView(R.id.add_chapter)
        CreateAndEditFragment.edit.text.append("Название")
        clickOnTextInViewInDialog(R.string.dialog_add)
        clickOnView(R.id.add_chapter)
        CreateAndEditFragment.edit.text.append("Название")
        clickOnTextInViewInDialog(R.string.dialog_add)
        clickOnView(R.id.add_chapter)
        CreateAndEditFragment.edit.text.append("Название")
        clickOnTextInViewInDialog(R.string.dialog_add)
        clickOnView(R.id.create)

        scenario.onActivity {
            val viewModel = ViewModelProvider(
                MainActivity.viewModelStore,
                MyViewModelFactory(MainActivity.api)
            )[BookViewModel::class.java]
            while (viewModel.liveBook.value == null) {
                sleep(10)
            }
            val book = viewModel.liveBook.value!!
            assertEquals(nameBook, book.name)
            assertEquals(annotate, book.annotate)
            assertEquals(genres.id, book.genre.id)
        }
    }
}