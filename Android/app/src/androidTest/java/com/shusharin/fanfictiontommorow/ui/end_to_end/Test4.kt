package com.shusharin.fanfictiontommorow.ui.end_to_end

import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontommorow.ui.end_to_end.common.DoubleRecyclerViewMatcher
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import org.junit.Assert.assertNotEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test4 : Base() {
    @Test
    fun flippingThroughThePagesOfChapter() {
        openBookInPopulate()

        clickOnView(R.id.read)
        val firstText = getInChapter(R.id.fullscreen_content)
        clickOnView(R.id.right)
        val secondText = getInChapter(R.id.fullscreen_content)
        assertNotEquals(firstText, secondText)
    }

}