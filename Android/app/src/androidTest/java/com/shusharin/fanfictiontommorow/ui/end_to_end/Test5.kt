package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.appcompat.app.AppCompatDelegate
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontomorrow.R
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class Test5 : Base() {
    @Test

    fun changingTheTheme() {
        openInDrawerLayout(R.id.nav_settings)
        clickOnViewStringResource(R.string.theme_app)
        clickOnTextInViewInDialog(getArrayResources(R.array.themes_entries, 0))
        scenario.onActivity {
            assertEquals(AppCompatDelegate.MODE_NIGHT_YES, AppCompatDelegate.getDefaultNightMode())
        }
    }
}