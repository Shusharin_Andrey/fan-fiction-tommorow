package com.shusharin.fanfictiontommorow.ui.end_to_end

import androidx.lifecycle.ViewModelProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.shusharin.fanfictiontommorow.ui.end_to_end.common.DoubleRecyclerViewMatcher
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.get_queue.GetLibraryBook
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusReadingBook
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.book.BookViewModel
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewModelFactory
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import junit.framework.TestCase.assertEquals
import org.hamcrest.CoreMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
class Test1 : BaseAuth() {

    @Test
    fun addingBookToLibrary() {
//        sleep(2000)
        onView(
            DoubleRecyclerViewMatcher(R.id.list).atPositionOnView(
                0,
                R.id.listBooks,
                0,
                R.id.cornerCon
            )
        ).check(matches(isDisplayed())).perform(click())

        clickOnView(R.id.lists)

        clickOnTextInViewInDialog(StatusReadingBook.READING.nameSRB)

        clickOnTextInViewInDialog(R.string.dialog_ok)

        var idBook = -1
        var getLibraryBook: GetLibraryBook
        var isWaiting = true
        scenario.onActivity { activity ->
            val viewModel = ViewModelProvider(
                MainActivity.viewModelStore,
                MyViewModelFactory(MainActivity.api)
            )[BookViewModel::class.java]
            idBook = viewModel.liveBook.value!!.id
            val api = MyApiServices(activity, activity.resources, {}, {})
            getLibraryBook = api.getLibraryBook()
            getLibraryBook.liveLibraryBook.observe(activity) {
                assertEquals(StatusReadingBook.READING.id, it.statusReadingBook.id)
                isWaiting = false
            }
            getLibraryBook.sendRequest(MainActivity.idUser, idBook)
        }
        while (isWaiting) {
            sleep(10)
        }

    }
}