package com.shusharin.fanfictiontomorrow.integration

import com.shusharin.fanfictiontomorrow.requests.isTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Test4 : Base() {
    @Test
    fun `Checking the receipt of the default book, when sending a non-existent id`() {
        isTest = true
        val idBook = -10
        val book = getBookClass(0, idBook)
        isTest = false
        Assertions.assertEquals("Книга не найдена", book.name)
        Assertions.assertEquals(" ", book.annotate)
        Assertions.assertEquals(1, book.genre.id)
        Assertions.assertEquals(1, book.completeness.id)
        Assertions.assertEquals(0, book.author.id)
        Assertions.assertEquals(0u, book.quantityOfViews)
        Assertions.assertEquals(0, book.idCoverBook)
        Assertions.assertFalse(book.isDraft)
        Assertions.assertEquals("0", book.date)
    }
}