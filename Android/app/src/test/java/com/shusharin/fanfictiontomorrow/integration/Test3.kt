package com.shusharin.fanfictiontomorrow.integration

import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genres
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Test3 : Base() {
    @Test
    fun `Checking that a book with parameters that go beyond the FS conditions has not been added`() {
        val name = ""
        val annotation = ""
        val bookClass = BookClass()
        bookClass.name = name
        bookClass.annotation = annotation

        val idBook = addBook(bookClass)
        Assertions.assertEquals(-1, idBook)

        for (genre in Genres.values()) {
            val bookInGenre = getListIdGenreBooks(genre.id)
            for (idBookResponse in bookInGenre) {
                try {
                    val book = getSearchBook(-1, idBookResponse.key, 1)
                    Assertions.assertNotEquals(name, book.name)
                    Assertions.assertNotEquals(annotation, book.annotate)
                } catch (_: UninitializedPropertyAccessException) {

                }
            }
        }
    }
}