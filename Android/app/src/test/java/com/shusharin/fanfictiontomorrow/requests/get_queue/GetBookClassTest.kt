package com.shusharin.fanfictiontomorrow.requests.get_queue

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import org.junit.jupiter.api.Assertions.assertTimeoutPreemptively
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetBookClassTest : RequestTestBase() {
    private lateinit var mockRequest: GetBookClass

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getBookClass)
    }

    @Test
    fun sendRequestFakeStatusReadingMore() {
        val bookClass = BookClass(bookDefaultResponse)
        val bookClassParams = BookClassParams(1, 1)
        sendRequest(mockRequest, bookClassParams, bookClass)
        assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val book = slotBook.captured
        slotBook.clear()

        assert(book.id == bookClassParams.idBook)
        assert(book.name == bookClass.name)
        assert(book.isDraft)
        assert(book.genre.id == bookClass.genreId)
        assert(book.idCoverBook == bookClass.coverId)
        assert(book.completeness.id == bookClass.completenessId)
        assert(book.date == bookClass.date)
        assert(book.annotate == bookClass.annotation)
        assert(book.quantityOfViews == bookClass.views.toUInt())
        assert(book.author.id == bookClass.authorId)
    }
}