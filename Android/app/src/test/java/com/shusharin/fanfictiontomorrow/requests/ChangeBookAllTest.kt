package com.shusharin.fanfictiontomorrow.requests

import android.graphics.Bitmap
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class ChangeBookAllTest : RequestTestBase() {
    private lateinit var mockRequest: ChangeBookAll

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::changeBookAll)
    }

    @Test
    fun sendRequestFakeTrue() {
        mockCall(true, mockRequest)
        val bitmap = mockk<Bitmap>()
        every { bitmap.compress(any(), any(), any()) } returns true

        mockRequest.sendRequest(
            bitmap,
            1,
            BookClass(),
            hashMapOf(Pair(1, Chapter(chapterInBookDefaultResponse))),
            hashMapOf(Pair(1, Chapter(chapterInBookDefaultResponse))),
            hashMapOf(Pair(1, Chapter(chapterInBookDefaultResponse))),
        )
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val boolean = slotBooleanChangeBookAll.captured
        assert(boolean)
    }
}