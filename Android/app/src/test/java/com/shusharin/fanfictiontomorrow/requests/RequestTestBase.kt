package com.shusharin.fanfictiontomorrow.requests

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.shusharin.fanfictiontomorrow.BaseTest
import com.shusharin.fanfictiontomorrow.requests.utils.classes.*
import io.mockk.*
import org.junit.jupiter.api.*
import kotlin.reflect.KFunction0

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
open class RequestTestBase : BaseTest() {

    protected fun setReturnNullBitmap() {
        every { BitmapFactory.decodeStream(any()) } returns null
        val bitmapMock = mockk<Bitmap>()
        every { BitmapFactory.decodeResource(any(), any()) } returns bitmapMock
    }

    protected fun verifyReturnNullBitmap() {
        verify { BitmapFactory.decodeStream(any()) }
        verify { BitmapFactory.decodeResource(any(), any()) }
    }

    protected inline fun <reified TypeRequest : Any> getRequest(getRequest: KFunction0<TypeRequest>): TypeRequest {
        isMockAll = false
        val mockRequest = getRequest()
        isMockAll = true
        return mockRequest
    }

    protected inline fun <reified RequestType : Any, reified RequestParams, RequestLiveData> sendRequest(
        request: MyRequest<RequestType, RequestParams, RequestLiveData>,
        requestParams: RequestParams,
        responseBody: RequestType?,
        typeSend: Int = 1,
    ) {
        val (mockedResponse, mockedCall) = mockCall(responseBody, request, typeSend)
        request.sendRequest(requestParams)
        verify { request.call(requestParams) }
        verify { mockedCall.clone() }
        verify { mockedCall.enqueue(any()) }
        if (typeSend == 1) {
            verify { mockedResponse.body() }
        }
    }
}