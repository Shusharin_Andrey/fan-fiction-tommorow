package com.shusharin.fanfictiontomorrow.requests.get_single

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genre
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListPresentBookInGenre
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetPresentBookInGenreTest : RequestTestBase() {
    private lateinit var mockRequest: GetPresentBookInGenre

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getPresentBookInGenre)
    }

    @Test
    fun sendRequestFake() {
        val genres = mutableListOf(
            Genre(0, true),
            Genre(1, false),
            Genre(2, true),
            Genre(3, false),
            Genre(4, true),
            Genre(5, false),
            Genre(6, false),
            Genre(7, false),
            Genre(8, true),
        )
        sendRequest(mockRequest, mockk(), ListPresentBookInGenre(genres))
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val genresResponse = slotGenres.captured
        assert(genresResponse[0].isPresent)
        assert(!genresResponse[1].isPresent)
        assert(genresResponse[2].isPresent)
        assert(!genresResponse[3].isPresent)
        assert(genresResponse[4].isPresent)
        assert(!genresResponse[5].isPresent)
        assert(!genresResponse[6].isPresent)
        assert(!genresResponse[7].isPresent)
        assert(genresResponse[8].isPresent)
    }
}