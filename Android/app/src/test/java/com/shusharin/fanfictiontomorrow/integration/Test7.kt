package com.shusharin.fanfictiontomorrow.integration

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Test7 : Base() {
    @Test
    fun `Checking the possibility of getting a book in the draft state by its author`() {
        val idUser = 1
        val idBook = 2
        val book = getBookClass(idUser, idBook)
        Assertions.assertEquals(idBook, book.id)
        Assertions.assertEquals(idUser, book.author.id)
        Assertions.assertTrue(book.isDraft)
    }
}