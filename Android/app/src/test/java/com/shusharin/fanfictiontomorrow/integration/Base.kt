package com.shusharin.fanfictiontomorrow.integration

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.isTest
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Book
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genre
import com.shusharin.fanfictiontomorrow.requests.utils.classes.ReadBook
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import java.time.Duration
import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter

open class Base : RequestTestBase() {

    @BeforeEach
    fun setUp() {
        isTest = true
    }

    fun addChapter(idBook: Int, chapter: Chapter): Int {
        isMockAll = false
        val mockAddChapter = mockkMyApiServices.addChapter()
        isMockAll = true
        mockAddChapter.sendRequest(idBook, chapter)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        return slotInt.captured
    }

    fun addBookInLibrary(idUser: Int, idStatus: Int, idBook: Int): Boolean {
        val mockAddBookInLibrary = getRequest(mockkMyApiServices::addBookInLibrary)
        mockAddBookInLibrary.sendRequest(idUser, idStatus, idBook)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        return slotBoolean.captured
    }

    fun addBook(bookClass: BookClass): Int {
        val mockAddBook = getRequest(mockkMyApiServices::addBook)
        mockAddBook.sendRequest(bookClass)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        return slotInt.captured
    }

    fun getChapter(idChapter: Int): Chapter {
        val mockGetChapter = getRequest(mockkMyApiServices::getChapter)
        mockGetChapter.sendRequest(idChapter)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(100)
            } while (quantityActiveRequests != 0)
        }
        return slotChapter.captured
    }

    fun getListIdBooksLibrary(idUser: Int): HashMap<Int, ReadBook> {
        val mockGetListIdBooksLibrary = getRequest(mockkMyApiServices::getListIdBooksLibrary)
        mockGetListIdBooksLibrary.sendRequest(idUser)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        return slotHashMapReadBook.captured
    }

    fun getListIdGenreBooks(idGenre: Int): HashMap<Int, FindBook> {
        val mockGetListIdGenreBooks = getRequest(mockkMyApiServices::getListIdGenreBooks)
        mockGetListIdGenreBooks.sendRequest(idGenre)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        return slotHashMapFindBook.captured
    }

    fun getListIdChapters(idBook: Int): HashMap<Int, Chapter> {
        val mockGetListIdChapters = getRequest(mockkMyApiServices::getListIdChapters)
        mockGetListIdChapters.sendRequest(idBook)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
            do {
                Thread.sleep(10)
            } while (!slotHashMapChapter.isCaptured)
        }
        return slotHashMapChapter.captured
    }

    fun getBookClass(idUser: Int, idBook: Int): Book {
        val mockGetBookClass = getRequest(mockkMyApiServices::getBookClass)
        mockGetBookClass.sendRequest(idUser, idBook)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        return slotBook.captured
    }

    fun getListIdBooksInSection(sections: Sections): HashMap<Int, FindBook> {
        val mockGetListIdBooksInSection = getRequest(mockkMyApiServices::getListIdBooksInSection)
        mockGetListIdBooksInSection.sendRequest(sections)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            while (quantityActiveRequests != 0) {
                Thread.sleep(10)
            }
        }
        return slotHashMapFindBook.captured
    }

    fun getLibraryBook(idUser: Int, idBook: Int): ReadBook {
        val mockGetLibraryBook = getRequest(mockkMyApiServices::getLibraryBook)
        mockGetLibraryBook.sendRequest(idUser, idBook)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        return slotReadBook.captured
    }


    fun getSearchBook(idUser: Int, idBook: Int, sortType: Int): FindBook {
        val mockRequest = getRequest(mockkMyApiServices::getSearchBook)
        mockRequest.sendRequest(idUser, idBook, sortType)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        return slotFindBook.captured
    }

    fun getPresentBookInGenre(): MutableList<Genre> {
        val mockGetPresentBookInGenre = getRequest(mockkMyApiServices::getPresentBookInGenre)
        mockGetPresentBookInGenre.sendRequest()
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
            do {
                Thread.sleep(10)
            } while (!slotGenres.isCaptured)
        }
        return slotGenres.captured
    }

    fun getLastChapter(idUser: Int, idBook: Int): Int {
        val mockGetLastChapter = getRequest(mockkMyApiServices::getLastChapter)
        mockGetLastChapter.sendRequest(idUser, idBook)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
            do {
                Thread.sleep(10)
            } while (!slotInt.isCaptured)
        }
        return slotInt.captured
    }

    fun getAntiquity(book: Book): Period {
        val from = LocalDate.parse(book.date, DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        val today = LocalDate.now()
        return Period.between(from, today)
    }
}