package com.shusharin.fanfictiontomorrow.integration

import com.shusharin.fanfictiontomorrow.requests.isTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Test8 : Base() {

    @Test
    fun `Checking if a book is unavailable in the draft state to users`() {
        isTest = false
        val idUser = 1
        val idBook = 47
        slotBook.clear()
        Assertions.assertThrows(UninitializedPropertyAccessException::class.java) {
            getBookClass(idUser, idBook)
        }
    }
}