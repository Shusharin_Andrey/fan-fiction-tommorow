package com.shusharin.fanfictiontomorrow.requests

import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import okhttp3.ResponseBody
import org.junit.jupiter.api.Test
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

internal open class MyRequestTest : RequestTestBase() {

    @InjectMockKs
    private var getCover = mockkMyApiServices.getCover()


    @Test
    fun sendRequestNullResponse() {
        val mockedResponse = mockk<Response<ResponseBody>>(relaxed = true)
        every { mockedResponse.body() } returns null
        sendRequest(mockedResponse, getCall(mockedResponse))
    }

    @Test
    fun sendRequestNotNullResponse() {
        val mockedResponse = mockk<Response<ResponseBody>>(relaxed = true)
        val mockedResponseBody = mockk<ResponseBody>(relaxed = true)
        every { mockedResponse.body() } returns mockedResponseBody
        sendRequest(mockedResponse, getCall(mockedResponse))
        verifyReturnNullBitmap()
    }

    @Test
    fun sendRequestFailure() {
        val mockedResponse = mockk<Response<ResponseBody>>(relaxed = true)
        val mockedResponseBody = mockk<ResponseBody>(relaxed = true)
        every { mockedResponse.body() } returns mockedResponseBody
        sendRequest(mockedResponse, getCall(mockedResponse, false), false)
    }

    private fun getCall(
        mockedResponse: Response<ResponseBody>,
        isSuccess: Boolean = true,
    ): Call<ResponseBody> {
        val mockedCall = mockk<Call<ResponseBody>>(relaxed = true)
        val mockedThrowable = mockk<Throwable>(relaxed = true)
        every { mockedThrowable.printStackTrace() } returns println("printStackTrace")
        every { mockedCall.clone() } returns mockedCall
        every { mockedCall.enqueue(any()) } answers {
            val callback = args[0] as Callback<ResponseBody>
            if (isSuccess) {
                callback.onResponse(mockedCall, mockedResponse)
            } else {
                callback.onFailure(mockedCall, mockedThrowable)
            }
        }
        return mockedCall
    }

    private fun sendRequest(
        mockedResponse: Response<ResponseBody>,
        mockedCall: Call<ResponseBody>,
        isSuccess: Boolean = true,
    ) {
        setReturnNullBitmap()

        val getCoverSpy = spyk(getCover, recordPrivateCalls = true)
        every {
            getCoverSpy.call(any())
        } returns mockedCall

        getCoverSpy.sendRequest(1)

        verify { getCoverSpy.call(any()) }
        verify { mockedCall.clone() }
        verify { mockedCall.enqueue(any()) }
        if (isSuccess) {
            verify { mockedResponse.body() }
        }
    }
}