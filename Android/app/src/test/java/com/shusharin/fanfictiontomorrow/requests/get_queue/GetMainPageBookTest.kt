package com.shusharin.fanfictiontomorrow.requests.get_queue

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.requests.utils.model.MainPageBook
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetMainPageBookTest : RequestTestBase() {
    private lateinit var mockRequest: GetMainPageBook

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getMainPageBook)
    }

    @Test
    fun sendRequestFake() {
        val mainPageBook = MainPageBook("Название", 1, 1, false)
        val mainPageBookParams = MainPageBookParams(Sections.LAST, 1)
        sendRequest(mockRequest, mainPageBookParams, mainPageBook)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val smallBook = slotSmallBook.captured

        assert(smallBook.id == mainPageBookParams.idBook)
        assert(smallBook.name == mainPageBook.name)
        assert(!smallBook.isDraft)
        assert(smallBook.idCoverBook == mainPageBook.coverId)
    }
}