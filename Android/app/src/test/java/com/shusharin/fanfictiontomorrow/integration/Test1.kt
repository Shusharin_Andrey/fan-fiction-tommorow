package com.shusharin.fanfictiontomorrow.integration

import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Test1 : Base() {
    @Test
    fun `Checking the successful addition of the book`() {
        val idBook = 1
        val chapter = Chapter("Название главы", "Текст главы")

        val idChapterResponse = addChapter(idBook, chapter)

        val chaptersInBookResponse = getListIdChapters(idBook)
        Assertions.assertTrue(chaptersInBookResponse.containsKey(idChapterResponse))
        Assertions.assertEquals(chapter.name, chaptersInBookResponse[idChapterResponse]!!.name)

        val chapterResponse = getChapter(idChapterResponse)
        Assertions.assertEquals(idChapterResponse, chapterResponse.id)
        Assertions.assertEquals(chapter.text, chapterResponse.text)
    }
}