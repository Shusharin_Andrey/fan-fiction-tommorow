package com.shusharin.fanfictiontomorrow.requests.get_list

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetListIdBooksInSectionTest : RequestTestBase() {
    private lateinit var mockRequest: GetListIdBooksInSection

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getListIdBooksInSection)
    }

    @Test
    fun sendRequestFake() {
        val listBookId = ListBookId()
        listBookId.booksId = arrayListOf(bookDefaultResponse.id)
        val listIdParams = ListIdBooksInSectionParams(Sections.POPULATE)
        sendRequest(mockRequest, listIdParams, listBookId)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val book = slotHashMapFindBook.captured[bookDefaultResponse.id]!!
        slotHashMapFindBook.clear()
        assert(book.id == 1)
    }
}