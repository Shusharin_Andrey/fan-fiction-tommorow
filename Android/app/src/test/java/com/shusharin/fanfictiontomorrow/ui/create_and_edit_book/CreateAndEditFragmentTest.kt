package com.shusharin.fanfictiontomorrow.ui.create_and_edit_book

import com.shusharin.fanfictiontomorrow.databinding.FragmentCreateAndEditBinding
import com.shusharin.fanfictiontomorrow.ui.FragmentBaseTest
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class CreateAndEditFragmentTest : FragmentBaseTest() {
    private lateinit var mockBookFragmentTest: CreateAndEditFragment

    @BeforeEach
    fun setUp() {
        setReturnNotNullBitmap()
        mockBookFragmentTest = spyk(CreateAndEditFragment(), recordPrivateCalls = true)
        mockkBase(mockBookFragmentTest)

        val binding = mockk<FragmentCreateAndEditBinding>()
        every { mockBookFragmentTest["getGenre"]() } returns textView
        every { mockBookFragmentTest["setStatusCreatedBook"]() } answers {}
        every { mockBookFragmentTest["setCheckedSwitchDraft"]() } answers {}
        every { mockBookFragmentTest["setInterfaceWithChapters"]() } answers {}
        every { mockBookFragmentTest["setDraft"]() } answers {}
        every { mockBookFragmentTest["setNameBookAndAnnotate"]() } answers {}
        every { mockBookFragmentTest["getStatusCreateBook"]() } returns textView
        every { mockBookFragmentTest["getCreate"]() } returns button
        every { mockBookFragmentTest["getAddChapter"]() } returns button
        every { mockBookFragmentTest["getCover"]() } returns imageView
        every { mockBookFragmentTest["getListChapter"]() } returns recyclerView
        every { mockBookFragmentTest["setClipToOutline"]() } answers {}
        every { mockBookFragmentTest.setBinding(any(), any()) } answers {}

        every {
            mockBookFragmentTest getProperty "binding"
        } propertyType FragmentCreateAndEditBinding::class answers {
            binding
        }
    }

    @Test
    fun onCreateViewBundleTrue() {
        every { bundle.getInt(any()) } returns 3
        every { bundle.getBoolean(any(), any()) } returns false
        mockBookFragmentTest.onCreateView(mockk(), mockk(), mockk())
        every { bundle.getBoolean(any(), any()) } returns true
        mockBookFragmentTest.onCreateView(mockk(), mockk(), mockk())
    }

    @Test
    fun onCreateViewBundleTrueIdBookNot() {
        every { bundle.getBoolean(any(), any()) } returns false
        every { bundle.getInt(any()) } returns -1
        mockBookFragmentTest.onCreateView(mockk(), mockk(), mockk())
        every { bundle.getBoolean(any(), any()) } returns true
        mockBookFragmentTest.onCreateView(mockk(), mockk(), mockk())
    }

    @Test
    fun onCreateViewBundleFalse() {
        every { bundle.getInt(any()) } returns 3
        every { bundle.getBoolean(any(), any()) } returns false
        mockBookFragmentTest.onCreateView(mockk(), mockk(), mockk())
    }
}