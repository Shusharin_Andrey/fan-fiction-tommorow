package com.shusharin.fanfictiontomorrow.requests.add_queue

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class AddChapterTest : RequestTestBase() {
    private lateinit var mockAddChapter: AddChapter

    @BeforeEach
    fun setUp() {
        isMockAll = false
        mockAddChapter = mockkMyApiServices.addChapter()
        isMockAll = true
    }

    @Test
    fun sendRequestFake() {
        val idChapter = 1
        val addChapterParams = mockk<AddChapterParams>()
        sendRequest(mockAddChapter, addChapterParams, idChapter)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val idChapterResponse = slotInt.captured
        assert(idChapterResponse == idChapter)
    }
}