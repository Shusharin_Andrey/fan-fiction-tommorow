package com.shusharin.fanfictiontomorrow.integration

import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusReadingBook
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Test2 : Base() {
    @Test
    fun `Checking the successful addition a book to the library`() {
        val idUser = 1
        val idBook = 1
        val idStatus = StatusReadingBook.READING.id

        val isBookAddedResponse = addBookInLibrary(idUser, idStatus, idBook)
        Assertions.assertTrue(isBookAddedResponse)

        val booksInLibrary = getListIdBooksLibrary(idUser)
        Assertions.assertTrue(booksInLibrary.containsKey(idBook))

        val readBook = getLibraryBook(idUser, idBook)
        Assertions.assertEquals(idStatus, readBook.statusReadingBook.id)

        addBookInLibrary(idUser, StatusReadingBook.READ_LATER.id, idBook)
    }
}