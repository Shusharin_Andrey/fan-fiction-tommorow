package com.shusharin.fanfictiontomorrow.ui.main_page

import com.shusharin.fanfictiontomorrow.requests.get_list.GetListIdBooksInSection
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.NOT_AUTH
import com.shusharin.fanfictiontomorrow.ui.parents.FragmentBaseRecycleTest
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.mockkConstructor
import io.mockk.spyk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class MainPageFragmentTest : FragmentBaseRecycleTest() {
    private lateinit var mockMainPageFragment: MainPageFragment

    @BeforeEach
    fun setUp() {
        setReturnNotNullBitmap()
        mockMainPageFragment = spyk(MainPageFragment(), recordPrivateCalls = true)
        mockkBase(mockMainPageFragment)
        mockkRecycleViewer(mockMainPageFragment)
    }

    @Test
    fun onCreateView() {
        mockMainPageFragment.onCreateView(mockk(), mockk(), mockk())
    }

    @Test
    fun onStart() {
        mockkConstructor(GetListIdBooksInSection::class)
        every { constructedWith<GetListIdBooksInSection>().sendRequest(any<Sections>()) } just Runs
        mockMainPageFragment.onCreateView(mockk(), mockk(), mockk())
        mockMainPageFragment.onStart()
    }

    @Test
    fun onStartUser() {
        MainActivity.idUser = 3
        mockMainPageFragment.onCreateView(mockk(), mockk(), mockk())
        mockMainPageFragment.onStart()
        MainActivity.idUser = NOT_AUTH
    }
}