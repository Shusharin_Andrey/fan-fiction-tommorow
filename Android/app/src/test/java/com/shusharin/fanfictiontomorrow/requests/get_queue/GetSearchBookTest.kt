package com.shusharin.fanfictiontomorrow.requests.get_queue

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.model.SearchBook
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetSearchBookTest : RequestTestBase() {
    private lateinit var mockRequest: GetSearchBook

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getSearchBook)
    }

    @Test
    fun sendRequestFake() {
        val searchBook = SearchBook("Название", 1, "Аннотация", 1, 1, 1, false)
        val searchBookParams = SearchBookParams(1, 1, 1)
        sendRequest(mockRequest, searchBookParams, searchBook)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val findBook = slotFindBook.captured
        assert(findBook.id == searchBookParams.idBook)
        assert(findBook.name == searchBook.name)
        assert(!findBook.isDraft)
        assert(findBook.annotate == searchBook.annotation)
        assert(findBook.genre.id == searchBook.genre)
        assert(findBook.quantityOfViews == searchBook.views.toUInt())
        assert(findBook.idCoverBook == searchBook.coverId)
        assert(findBook.sort == searchBook.sort)
    }
}