package com.shusharin.fanfictiontomorrow.integration

import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Test9 : Base() {
    @Test
    fun `Checking the opening of the book on chapter 1, if the user has not read it before`() {
        val idUser = 2
        val idBook = 1
        val idChapter = getLastChapter(idUser, idBook)
        val chapters = getListIdChapters(idBook)
        val sortChapters = Chapter.convertToArrayAndSort(chapters)
        Assertions.assertEquals(idChapter, sortChapters[0].first)
    }
}