package com.shusharin.fanfictiontomorrow.requests.get_single

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class SetDraftTest : RequestTestBase() {
    private lateinit var mockRequest: SetDraft

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::setDraft)
    }

    @Test
    fun sendRequestFakeTrue() {
        sendRequest(mockRequest, SetDraftParams(1), true)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val boolean = slotBoolean.captured
        assert(boolean)
    }

    @Test
    fun sendRequestFakeFalse() {
        sendRequest(mockRequest, SetDraftParams(1), false)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val boolean = slotBoolean.captured
        assert(!boolean)
    }
}