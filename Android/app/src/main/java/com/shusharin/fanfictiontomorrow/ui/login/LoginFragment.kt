package com.shusharin.fanfictiontomorrow.ui.login

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.preference.PreferenceManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.databinding.FragmentLoginBinding
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewFragment


class LoginFragment : MyViewFragment<LoginViewModel, FragmentLoginBinding>() {
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private var isSuccessSignIn: Boolean = false
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<LoginViewModel>()
        val root: View = binding.root
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestIdToken(getString(R.string.google_id_token))
            .build()

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)

        binding.signUpButton.setOnClickListener {
            mGoogleSignInClient!!.signOut()
                .addOnCompleteListener(requireActivity()) {
                    val signInIntent = mGoogleSignInClient!!.signInIntent
                    resultLauncher.launch(signInIntent)
                }
        }

        binding.signInButton.setOnClickListener {
            val account = activity?.let { GoogleSignIn.getLastSignedInAccount(it) }
            if (account != null) {
                updateUI(account)
            } else {
                val signInIntent = mGoogleSignInClient!!.signInIntent
                resultLauncher.launch(signInIntent)
            }
        }

        viewModel.liveIdUser.observe(viewLifecycleOwner) { idUser ->
            println("IdUser = $idUser")
            if (idUser != -1) {
                val sharedPreferences: SharedPreferences? =
                    activity?.let { PreferenceManager.getDefaultSharedPreferences(it) }
                val edit = sharedPreferences!!.edit()
                edit.putInt(getString(R.string.id_user), idUser)
                edit.apply()

                MainActivity.idUser = idUser
                MainActivity.menu.findItem(R.id.nav_logout).isVisible = true
                MainActivity.menu.findItem(R.id.nav_login).isVisible = false
                MainActivity.navigate(R.id.nav_main_page)
            }
        }

        return root
    }

    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            isSuccessSignIn = result.resultCode == Activity.RESULT_OK

            if (isSuccessSignIn) {
                val data: Intent? = result.data
                val task: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)
            }
        }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            updateUI(account)
        } catch (e: ApiException) {
            updateUI(null)
        }
    }

    private fun updateUI(account: GoogleSignInAccount?) {
        if (account != null) {

            val email = account.email.toString()
            val firstName = account.givenName.toString()
            val lastName = account.familyName.toString()

            viewModel.getAuthUserId.sendRequest(firstName, lastName, email)
            update()
        } else {
            println("account is null")
            Toast.makeText(activity, "account is null", Toast.LENGTH_LONG).show()
        }
    }

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
    }

    override fun setSwipeRefreshLayout() {
        swipeRefreshLayout = binding.swipeRefreshLayout
    }

    override fun addFragmentToStack() {
    }
}