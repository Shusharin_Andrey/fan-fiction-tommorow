package com.shusharin.fanfictiontomorrow.requests

import android.graphics.Bitmap
import androidx.lifecycle.LifecycleOwner
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Book
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.requests.utils.classes.ReadBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import com.shusharin.fanfictiontomorrow.requests.utils.model.Person
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import java.util.*

interface IRequests {
    val owner: LifecycleOwner
    val api: MyApiServices
}

interface IGetUser : IRequests {
    fun getUser(idUser: Int) {
        val getUser = api.getUser()
        getUser.livePerson.observe(owner) {
            if (it.id != -1) {
                setAuthor(it)
            }
        }
        getUser.sendRequest(idUser)
    }

    fun setAuthor(it: Person)
}

interface IAddCover : IRequests {

    fun addCover(
        bitmap: Bitmap,
        bookClass: BookClass,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    ) {
        val addCover = api.addCover()
        addCover.liveIdCover.observe(owner) { idCover ->
            if (idCover != -1) {
                setIdCover(idCover, bookClass, deleteChapters, addedChapters, changesChapters)
            }
        }
        addCover.sendRequest(bitmap)
    }

    fun setIdCover(
        idCover: Int,
        bookClass: BookClass,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    )
}

interface IChangeCover : IRequests {

    fun changeCover(
        bitmap: Bitmap,
        idBookNew: Int,
        bookClass: BookClass,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    ) {
        val changeCover = api.changeCover()
        changeCover.liveIsChanged.observe(owner) {
            if (it) {
                setIsSuccessChangeCover(
                    it,
                    idBookNew,
                    bookClass,
                    deleteChapters,
                    addedChapters,
                    changesChapters
                )
            }
        }
        changeCover.sendRequest(bookClass.coverId, bitmap)
    }

    fun setIsSuccessChangeCover(
        it: Boolean,
        idBookNew: Int,
        bookClass: BookClass,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    )
}

interface IAddBook : IRequests {

    fun addBook(
        bookClass: BookClass,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    ) {
        val addBook = api.addBook()
        addBook.liveIdBook.observe(owner) { idBook ->
            if (idBook != -1) {
                setIdBook(idBook, deleteChapters, addedChapters, changesChapters)
            }
        }
        addBook.sendRequest(bookClass)
    }

    fun setIdBook(
        idBook: Int,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    )
}

interface IChangeBook : IRequests {

    fun changeBook(
        idBookNew: Int,
        bookClass: BookClass,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    ) {
        val changeBook = api.changeBook()
        changeBook.idBook.observe(owner) { idBook ->
            if (idBook != -1) {
                setIdBook(idBook, deleteChapters, addedChapters, changesChapters)
            }
        }
        changeBook.sendRequest(idBookNew, bookClass)
    }

    fun setIdBook(
        idBook: Int,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    )
}

interface IDeleteChapter : IRequestsQueueChapter {
    fun deleteChapter(
        idBook: Int,
        deleteChapters: HashMap<Int, Chapter>,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    ) {
        println("IDeleteChapter deleteChapter")
        println(idBook)
        println(deleteChapters)
        println(addedChapters)
        println(changesChapters)
        sendRequestQueue(createQueue(deleteChapters), idBook, addedChapters, changesChapters)
    }

    override fun sendRequest(
        queue: Queue<IRequestsQueue.ListIdWriteBooksParams<Chapter>>,
        additionalParameters: ArrayList<Any>,
    ) {
        val params = queue.poll()!!
        val deleteChapter = api.deleteChapter()
        deleteChapter.isDelete.observe(owner) {
            if (it) {
                setSuccessDeleteChapter(it)
            }
            sendRequestQueue(queue, additionalParameters)
        }
        deleteChapter.sendRequest(params.firstParams)
    }

    fun setSuccessDeleteChapter(it: Boolean) {}

    override fun completed(
        additionalParameters: ArrayList<Any>,
    ) {
        completedDeleteChapters(
            additionalParameters[0] as Int,
            additionalParameters[1] as HashMap<Int, Chapter>,
            additionalParameters[2] as HashMap<Int, Chapter>,
        )
    }

    fun completedDeleteChapters(
        idBook: Int,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    )
}

interface IAddChapter :
    IRequestsQueueChapter {
    fun addChapter(
        idBook: Int,
        addedChapters: HashMap<Int, Chapter>,
        changesChapters: HashMap<Int, Chapter>,
    ) {
        sendRequestQueue(createQueue(addedChapters, idBook), changesChapters)
    }

    override fun sendRequest(
        queue: Queue<IRequestsQueue.ListIdWriteBooksParams<Chapter>>,
        additionalParameters: ArrayList<Any>,
    ) {
        val params = queue.poll()!!
        val addChapter = api.addChapter()
        addChapter.liveIdChapter.observe(owner) {
            if (it != -1) {
                setIdChapter(it)
            }
            sendRequestQueue(queue, additionalParameters)
        }
        addChapter.sendRequest(params.idBook, params.firstParams)
    }

    fun setIdChapter(it: Int) {}

    override fun completed(
        additionalParameters: ArrayList<Any>,
    ) {
        completedAddChapters(
            additionalParameters[0] as HashMap<Int, Chapter>
        )
    }

    fun completedAddChapters(
        changesChapters: HashMap<Int, Chapter>,
    )
}

interface IChangeChapter :
    IRequestsQueueChapter {
    fun changeChapter(
        changesChapters: HashMap<Int, Chapter>,
    ) {
        sendRequestQueue(createQueue(changesChapters))
    }

    override fun sendRequest(
        queue: Queue<IRequestsQueue.ListIdWriteBooksParams<Chapter>>,
        additionalParameters: ArrayList<Any>,
    ) {
        val params = queue.poll()!!
        val changeChapter = api.changeChapter()
        changeChapter.liveIsChanged.observe(owner) {
            if (it) {
                setIsSuccessChangeChapter(it)
            }
            sendRequestQueue(queue, additionalParameters)
        }
        changeChapter.sendRequest(params.firstParams)
    }

    fun setIsSuccessChangeChapter(it: Boolean) {}

    override fun completed(
        additionalParameters: ArrayList<Any>,
    ) {
        completedAddChapters()
    }

    fun completedAddChapters()
}

interface IGetCover : IRequests {
    fun getCover(idCover: Int) {
        if (idCover > 0) {
            val getCover = api.getCover()
            getCover.liveBitmap.observe(owner) {
                setCover(it)
            }
            getCover.sendRequest(idCover)
        }
    }

    fun setCover(it: Bitmap)
}

interface IGetLibraryBook :
    IRequestsQueue<Int> {

    fun getLibraryBook(idUser: Int, listBookId: ListBookId) {
        sendRequestQueue(createQueue(idUser, listBookId))
    }

    fun getLibraryBook(idUser: Int, idBook: Int) {
        sendRequestQueue(createQueue(idUser, idBook))
    }

    override fun sendRequest(
        queue: Queue<IRequestsQueue.ListIdWriteBooksParams<Int>>,
        additionalParameters: ArrayList<Any>,
    ) {
        val params = queue.poll()!!
        val getLibraryBook = api.getLibraryBook()
        getLibraryBook.liveLibraryBook.observe(owner) {
            if (it.id != -1) {
                setLibraryBook(it)
            }
            sendRequestQueue(queue, additionalParameters)
        }
        getLibraryBook.sendRequest(params.firstParams, params.idBook)
    }

    fun setLibraryBook(it: ReadBook)
}

interface IGetLastChapter : IRequests {
    fun getLastChapter(idUser: Int, idBook: Int) {
        val getLastChapter = api.getLastChapter()
        getLastChapter.liveIdLastChapter.observe(owner) {
            if (it != -1) {
                setLastChapter(it)
            }
        }
        getLastChapter.sendRequest(idUser, idBook)
    }

    fun setLastChapter(it: Int)
}

interface IRequestsQueue<FirstParams> :
    IRequests {
    class ListIdWriteBooksParams<FirstParams>(
        val firstParams: FirstParams,
        val idBook: Int,
        val sortType: Int = 0,
    )

    fun createQueue(
        firstParams: FirstParams,
        listBooksId: ListBookId,
        sortType: Int = 0,
    ): Queue<ListIdWriteBooksParams<FirstParams>> {
        val queue: Queue<ListIdWriteBooksParams<FirstParams>> =
            LinkedList()
        for (idBook in listBooksId.booksId) {
            queue.add(
                ListIdWriteBooksParams(
                    firstParams,
                    idBook,
                    sortType
                )
            )
        }
        return queue
    }

    fun createQueue(
        firstParams: FirstParams,
        idBook: Int,
        sortType: Int = 0,
    ): Queue<ListIdWriteBooksParams<FirstParams>> {
        val queue: Queue<ListIdWriteBooksParams<FirstParams>> =
            LinkedList()
        queue.add(
            ListIdWriteBooksParams(
                firstParams,
                idBook,
                sortType
            )
        )
        return queue
    }

    fun sendRequest(
        queue: Queue<ListIdWriteBooksParams<FirstParams>>,
        additionalParameters: ArrayList<Any>,
    )

    fun sendRequestQueue(
        queue: Queue<ListIdWriteBooksParams<FirstParams>>,
        vararg additionalParameters: Any,
    ) {
        val array = ArrayList<Any>()
        for (i in additionalParameters) {
            array.add(i)
        }
        sendRequestQueue(queue, array)
    }

    fun sendRequestQueue(
        queue: Queue<ListIdWriteBooksParams<FirstParams>>,
        additionalParameters: ArrayList<Any> = ArrayList(),
    ) {
        if (queue.isEmpty()) {
            try {
                completed(
                    additionalParameters
                )
            } catch (e: Exception) {
                println(e)
                e.printStackTrace()
            }
            return
        }
        val params = queue.peek()
        if (params == null) {
            queue.poll()
            sendRequestQueue(queue, additionalParameters)
        } else {
            sendRequest(queue, additionalParameters)
        }
    }

    fun completed(additionalParameters: ArrayList<Any>) {}


}

interface IRequestsQueueChapter :
    IRequestsQueue<Chapter> {
    fun createQueue(
        chapters: HashMap<Int, Chapter>,
        idBook: Int = -1,
    ): Queue<IRequestsQueue.ListIdWriteBooksParams<Chapter>> {
        val queue: Queue<IRequestsQueue.ListIdWriteBooksParams<Chapter>> =
            LinkedList()
        for (chapter in Chapter.convertToArrayAndSort(chapters)) {
            queue.add(
                IRequestsQueue.ListIdWriteBooksParams(
                    chapter.second,
                    idBook
                )
            )
        }
        return queue
    }
}

interface IGetBookClass :
    IRequestsQueue<Int> {
    fun getGetBookClass(idUser: Int, listBookId: ListBookId) {
        sendRequestQueue(createQueue(idUser, listBookId))
    }

    override fun sendRequest(
        queue: Queue<IRequestsQueue.ListIdWriteBooksParams<Int>>,
        additionalParameters: ArrayList<Any>,
    ) {
        val params = queue.poll()!!
        val getBookClass = api.getBookClass()
        getBookClass.liveBook.observe(owner) {
            if (it.id != -1) {
                setBookClass(it)
            }
            sendRequestQueue(queue, additionalParameters)
        }
        getBookClass.sendRequest(params.firstParams, params.idBook)
    }

    fun setBookClass(it: Book)
}

interface IGetSearchBook :
    IRequestsQueue<Int> {
    fun getSearchBook(idUser: Int, listBookId: ListBookId, sortType: Int) {
        sendRequestQueue(createQueue(idUser, listBookId, sortType))
    }

    fun getSearchBook(idUser: Int, idBook: Int, sortType: Int) {
        sendRequestQueue(createQueue(idUser, idBook, sortType))
    }

    override fun sendRequest(
        queue: Queue<IRequestsQueue.ListIdWriteBooksParams<Int>>,
        additionalParameters: ArrayList<Any>,
    ) {
        val params = queue.poll()!!
        val getSearchBook = api.getSearchBook()
        getSearchBook.liveFindBook.observe(owner) {
            if (it.id != -1) {
                setSearchBook(it)
            }
            sendRequestQueue(queue, additionalParameters)
        }
        getSearchBook.sendRequest(params.firstParams, params.idBook, params.sortType)
    }

    fun setSearchBook(it: FindBook)
}

