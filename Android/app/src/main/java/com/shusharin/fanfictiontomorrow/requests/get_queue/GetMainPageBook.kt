package com.shusharin.fanfictiontomorrow.requests.get_queue

import android.content.res.Resources
import android.graphics.Bitmap
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.requests.utils.classes.SmallBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.MainPageBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.Person
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import retrofit2.Call

class MainPageBookParams(val section: Sections, val idBook: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "MainPageBookParams(idBook=$idBook, section=$section)"
    }
}

class GetMainPageBook(
    owner: LifecycleOwner,
    api: MyApiServices,
    resources: Resources,
    startRefresh: () -> Unit,
    stopRefresh: () -> Unit,
) :
    GetBook<MainPageBook, MainPageBookParams, Sections, SmallBook>(
        owner, api, resources,
        startRefresh, stopRefresh
    ) {
    var mainPageBook = SmallBook(resources)
    val liveMainPageBook: MutableLiveData<SmallBook>
        get() = liveData

    override fun analysisOfResponseBody(
        responseBody: MainPageBook,
        params: MainPageBookParams,
    ) {
        if (!responseBody.isDraft) {
            mainPageBook = SmallBook(params.idBook, responseBody, resources)
            updateLiveData()
            getUser(responseBody.authorId)
            getCover(responseBody.coverId)
        }
    }

    override fun call(params: MainPageBookParams): Call<MainPageBook> {
        return service.getMainPageBook(params.idBook)
    }

    private fun updateLiveData() {
        setLiveData(mainPageBook)
    }

    override fun createRequestParams(firstParams: Sections, idBook: Int): MainPageBookParams {
        return MainPageBookParams(firstParams, idBook)
    }

    override fun setAuthor(it: Person) {
        liveMainPageBook.value!!.author = it
        updateLiveData()
    }

    override fun setCover(it: Bitmap) {
        liveMainPageBook.value!!.bitmap = it
        updateLiveData()
    }

}