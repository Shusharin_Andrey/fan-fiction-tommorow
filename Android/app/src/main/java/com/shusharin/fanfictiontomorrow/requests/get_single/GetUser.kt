package com.shusharin.fanfictiontomorrow.requests.get_single

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestIdentity
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.requests.utils.model.Person
import retrofit2.Call

class UserParams(val idUser: Int, val idBook: Int = 0, val section: Sections = Sections.LAST) :
    MyRequestParams() {
    override fun toString(): String {
        return "UserParams(idUser=$idUser, idBook=$idBook, section=$section)"
    }
}

class GetUser(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) :
    MyRequestIdentity<Person, UserParams>(startRefresh, stopRefresh) {
    val livePerson: MutableLiveData<Person>
        get() = liveData

    fun sendRequest(idUser: Int) {
        println("sendRequest idUser$idUser")
        sendRequest(UserParams(idUser))

    }

    override fun analysisOfResponseBody(responseBody: Person, params: UserParams) {
        responseBody.id = params.idUser
        setLiveData(responseBody)
    }

    override fun call(params: UserParams): Call<Person> {
        return service.getUser(params.idUser)
    }

}