package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.annotation.JsonProperty

class SearchBook {
    @JsonProperty
    var name: String = ""

    @JsonProperty
    var authorId = 0

    @JsonProperty
    var annotation: String = ""

    @JsonProperty
    var genre = 0

    @JsonProperty
    var views = 0

    @JsonProperty
    var coverId = 0

    @JsonProperty
    var isDraft = false

    @JsonProperty
    var sort = 0L

    constructor()

    constructor(
        name: String, authorId: Int, annotation: String,
        genre: Int, views: Int, coverId: Int, isDraft: Boolean
    ) {
        this.name = name
        this.authorId = authorId
        this.annotation = annotation
        this.genre = genre
        this.views = views
        this.coverId = coverId
        this.isDraft = isDraft
    }

    override fun toString(): String {
        return "SearchBook(name='$name', authorId=$authorId, annotation='$annotation', genre=$genre, views=$views, coverId=$coverId, isDraft=$isDraft, sort=$sort)"
    }


}