package com.shusharin.fanfictiontomorrow.ui.read_book

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.databinding.FragmentReadBookBinding
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewFragment

class ReadBookFragment : MyViewFragment<MyReadBookViewModel, FragmentReadBookBinding>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        requireActivity().window
        requireActivity().window.addFlags(
            WindowManager.LayoutParams.FLAG_SECURE
        )
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
    }

    @RequiresApi(Build.VERSION_CODES.R)
    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViewModel<MyReadBookViewModel>()

        val windowInsetsController =
            WindowCompat.getInsetsController(MainActivity.myWindow, MainActivity.myWindow.decorView)
        // Configure the behavior of the hidden system bars.
        windowInsetsController.systemBarsBehavior =
            WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE


        // Add a listener to update the behavior of the toggle fullscreen button when
        // the system bars are hidden or revealed.
        MainActivity.myWindow.decorView.setOnApplyWindowInsetsListener { _, windowInsets ->
            // You can hide the caption bar even when the other system bars are visible.
            // To account for this, explicitly check the visibility of navigationBars()
            // and statusBars() rather than checking the visibility of systemBars().
            if (windowInsets.isVisible(WindowInsetsCompat.Type.navigationBars())) {
                binding.central.setOnClickListener {
                    // Hide both the status bar and the navigation bar.
                    windowInsetsController.hide(WindowInsetsCompat.Type.navigationBars())
                    MainActivity.mySupportActionBar.hide()
                }
            } else {
                binding.central.setOnClickListener {
                    // Show both the status bar and the navigation bar.
                    windowInsetsController.show(WindowInsetsCompat.Type.navigationBars())
                    MainActivity.mySupportActionBar.show()
                }
            }
            view.onApplyWindowInsets(windowInsets)
        }


        viewModel.idBook = arguments?.getInt(getString(R.string.id_book))!!
        viewModel.idLastChapter = requireArguments().getInt(getString(R.string.id_chapter))


        binding.left.setOnClickListener {
            if (viewModel.page == 0) {
                if (viewModel.listIterator.hasPrevious()) {
                    viewModel.chapter = viewModel.listIterator.previous().second
                    if (viewModel.idLastChapter == viewModel.chapter.id) {
                        if (viewModel.listIterator.hasPrevious()) {
                            viewModel.chapter = viewModel.listIterator.previous().second
                            viewModel.page = -1
                        }
                    } else {
                        viewModel.page = -1
                    }
                    viewModel.idLastChapter = viewModel.chapter.id
                    viewModel.getChapter.sendRequest(viewModel.idLastChapter)
                }
            } else {
                viewModel.page--
                refreshText()
            }

        }

        binding.right.setOnClickListener {
            if (viewModel.page == viewModel.maxPage) {
                if (viewModel.listIterator.hasNext()) {
                    viewModel.chapter = viewModel.listIterator.next().second
                    if (viewModel.idLastChapter == viewModel.chapter.id) {
                        if (viewModel.listIterator.hasNext()) {
                            viewModel.chapter = viewModel.listIterator.next().second
                            viewModel.page = 0
                        }
                    } else {
                        viewModel.page = 0
                    }
                    viewModel.idLastChapter = viewModel.chapter.id
                    viewModel.getChapter.sendRequest(viewModel.idLastChapter)
                }
            } else {
                viewModel.page++
                refreshText()
            }
        }

        viewModel.liveChapters.observe(viewLifecycleOwner) { chaptersHashMap ->
            if (chaptersHashMap.isNotEmpty()) {
                val chaptersInBook = chaptersHashMap.toList()
                chaptersInBook.sortedBy { it.second.numberInBook }
                var iterator = chaptersInBook.listIterator()
                var chapter: Chapter? = null
                while (iterator.hasNext()) {
                    chapter = iterator.next().second
                    if (chapter.id == viewModel.idLastChapter) break
                }
                if (chapter == null) {
                    iterator = chaptersInBook.listIterator()
                    chapter = chaptersInBook[0].second
                }
                viewModel.chapter = chapter
                ("nameChapter in observe chapters= " + viewModel.chapter.name)
                viewModel.listIterator = iterator
                viewModel.getChapter.sendRequest(chapter.id)
            }
        }

        viewModel.liveChapter.observe(viewLifecycleOwner) {
            val nameChapter = viewModel.chapter.name
            viewModel.chapter = it
            viewModel.chapter.name = nameChapter
            var text = "W"
            while (!isTooLarge(text)) {
                text += "W"
            }
            val maxSymbols = (text.length - 2) * 35 - 1
            var nowNumberChar = 0
            viewModel.textPages.clear()
            while (nowNumberChar < viewModel.chapter.text.length) {
                val endNumberSymbol = getNumberEndSymbol(nowNumberChar, maxSymbols)
                viewModel.textPages.add(
                    viewModel.chapter.text.substring(
                        nowNumberChar, endNumberSymbol
                    )
                )
                nowNumberChar = endNumberSymbol + 1
            }
            viewModel.maxPage = viewModel.textPages.size - 1
            refreshText()
        }

        binding.right.setOnLongClickListener {
            onLongClick()
        }

        binding.left.setOnLongClickListener {
            onLongClick()
        }

        binding.central.setOnLongClickListener {
            onLongClick()
        }

        update()
        refresh()
    }

    @SuppressLint("SuspiciousIndentation")
    private fun onLongClick(): Boolean {
        val toast = Toast.makeText(
            requireContext(),
            "Копировать текст запрещено",
            Toast.LENGTH_SHORT
        )
        toast.show()
        return true
    }

    override fun update() {
        viewModel.getListIdChapters.sendRequest(viewModel.idBook)
    }

    private fun getNumberEndSymbol(nowNumberChar: Int, maxSymbols: Int) =
        if (nowNumberChar + maxSymbols < viewModel.chapter.text.length) {
            var end = nowNumberChar + maxSymbols
            while (viewModel.chapter.text[end] != ' ') {
                end--
            }
            end
        } else {
            viewModel.chapter.text.length
        }

    private fun refreshText() {
        if (viewModel.page == -1) {
            viewModel.page = viewModel.maxPage
        }
        MainActivity.toolbar.title = viewModel.chapter.name
        if (viewModel.textPages.isNotEmpty()) {
            binding.fullscreenContent.text = viewModel.textPages[viewModel.page]
        }
    }

    private fun isTooLarge(newText: String): Boolean {
        val textWidth = binding.fullscreenContent.paint.measureText(newText)
        return textWidth >= binding.fullscreenContent.measuredWidth
    }

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = FragmentReadBookBinding.inflate(inflater, container, false)
    }

    override fun setSwipeRefreshLayout() {
        swipeRefreshLayout = binding.swipeRefreshLayout
    }

    override fun addFragmentToStack() {
        MainActivity.stack.addLast(MainActivity.Companion.Nav(R.id.nav_read_book, arguments))
    }
}