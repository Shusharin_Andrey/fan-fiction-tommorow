package com.shusharin.fanfictiontomorrow.requests.get_list

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.IGetSearchBook
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import retrofit2.Call

class ListIdLastBooksParams(val idUser: Int = MainActivity.idUser) :
    MyRequestParams() {
    override fun toString(): String {
        return "ListIdLastBooksParams(idUser=$idUser)"
    }
}

class GetListIdLastBooks(
    override val owner: LifecycleOwner, override val api: MyApiServices, startRefresh: () -> Unit,
    stopRefresh: () -> Unit,
) : GetListIdBook<ListIdLastBooksParams, FindBook>(
    startRefresh,
    stopRefresh
), IGetSearchBook {
    val liveLastBooks: MutableLiveData<HashMap<Int, FindBook>>
        get() = liveData

    fun sendRequest(idUser: Int = this.idUser) {
        data.clear()
        sendRequest(ListIdLastBooksParams(idUser))
    }

    override fun analysisOfResponseBody(responseBody: ListBookId, params: ListIdLastBooksParams) {
        getSearchBook(params.idUser, responseBody, 1)
    }

    override fun call(params: ListIdLastBooksParams): Call<ListBookId> {
        return service.getLastBooks(params.idUser)
    }

    override fun setSearchBook(it: FindBook) {
        setLiveData(it)
    }
}