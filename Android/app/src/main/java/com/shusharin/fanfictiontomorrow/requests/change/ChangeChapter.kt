package com.shusharin.fanfictiontomorrow.requests.change

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestIdentity
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.model.ChapterSmall
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call

class ChangeChapterParams(val chapter: Chapter) : MyRequestParams() {
    override fun toString(): String {
        return "ChangeChapterParams(chapter=$chapter)"
    }
}

class ChangeChapter(startRefresh: () -> Unit, stopRefresh: () -> Unit) :
    MyRequestIdentity<Boolean, ChangeChapterParams>(startRefresh, stopRefresh) {
    val liveIsChanged: MutableLiveData<Boolean>
        get() = liveData

    fun sendRequest(chapter: Chapter) {
        sendRequest(ChangeChapterParams(chapter))
    }

    override fun call(params: ChangeChapterParams): Call<Boolean> {
        val requestBody = RequestBody.create(
            MediaType.get("application/json; charset=utf-8"),
            ChapterSmall(params.chapter).toString()
        )
        return service.changeChapter(params.chapter.id, requestBody)
    }
}