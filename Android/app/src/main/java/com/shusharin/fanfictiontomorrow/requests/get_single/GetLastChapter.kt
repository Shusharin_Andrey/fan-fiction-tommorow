package com.shusharin.fanfictiontomorrow.requests.get_single

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestIdentity
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import retrofit2.Call

class LastChapterParams(val idUser: Int = MainActivity.idUser, val idBook: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "LastChapterParams(idUser=$idUser, idBook=$idBook)"
    }
}

class GetLastChapter(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : MyRequestIdentity<Int, LastChapterParams>(startRefresh, stopRefresh) {
    val liveIdLastChapter: MutableLiveData<Int>
        get() = liveData

    fun sendRequest(idUser: Int = MainActivity.idUser, idBook: Int) {
        sendRequest(LastChapterParams(idUser, idBook))
    }

    override fun call(params: LastChapterParams): Call<Int> {
        return service.getLastChapter(params.idUser, params.idBook)
    }
}