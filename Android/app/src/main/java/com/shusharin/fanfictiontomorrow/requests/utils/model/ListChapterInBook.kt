package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize

class ListChapterInBook {
    @JsonDeserialize(`as` = ArrayList::class, contentAs = ChaptersInBook::class)
    var chapters: ArrayList<ChaptersInBook> = ArrayList()

}