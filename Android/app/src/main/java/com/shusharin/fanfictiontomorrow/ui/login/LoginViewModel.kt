package com.shusharin.fanfictiontomorrow.ui.login

import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class LoginViewModel(api: MyApiServices) : ViewModelUpdatable(api) {
    val getAuthUserId = api.getAuthUserId()
    val liveIdUser = getAuthUserId.liveIdUser
}