package com.shusharin.fanfictiontomorrow.requests.get_queue

import android.content.res.Resources
import androidx.lifecycle.LifecycleOwner
import com.shusharin.fanfictiontomorrow.requests.IGetCover
import com.shusharin.fanfictiontomorrow.requests.IGetUser
import com.shusharin.fanfictiontomorrow.requests.MyRequest
import com.shusharin.fanfictiontomorrow.utils.MyApiServices


abstract class GetBook<RequestType, RequestParams : com.shusharin.fanfictiontomorrow.requests.MyRequestParams, FirstParams, RequestLiveData>(
    final override val owner: LifecycleOwner,override val api: MyApiServices,
    val resources: Resources,
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : MyRequest<RequestType, RequestParams, RequestLiveData>(startRefresh, stopRefresh),
    IGetUser,
    IGetCover {

    fun sendRequest(idUser: FirstParams, idBook: Int) {
        sendRequest(createRequestParams(idUser, idBook))
    }

    abstract fun createRequestParams(firstParams: FirstParams, idBook: Int): RequestParams

}