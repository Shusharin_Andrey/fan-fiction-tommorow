package com.shusharin.fanfictiontomorrow.ui.utils

enum class Sorting(val id: Int, val nameSoring: String) {
    BY_POPULARITY(1, "По популярности"),
    BY_NOVELTY(2, "По новизне"),
}