package com.shusharin.fanfictiontomorrow.ui.create_and_edit_book

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.databinding.FragmentCreateAndEditBinding
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Book
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genres
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusCreatedBooks
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.create_and_edit_book.adapters.AdapterChaptersEdit
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewFragment
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogSelectionList
import java.util.*
import kotlin.collections.set


class CreateAndEditFragment :
    MyViewFragment<CreateAndEditViewModel, FragmentCreateAndEditBinding>() {
    private lateinit var nameGenres: Array<String>
    private lateinit var nameStatusCreatedBook: Array<String>

    companion object {
        //необходимо посмотреть, как поделиться viewModel с изменением главы
        lateinit var book: Book
        lateinit var edit: EditText
        var isCreated: Boolean = true
        var deleteChapters = HashMap<Int, Chapter>()
        var addedChapters = HashMap<Int, Chapter>()
        var changesChapters = HashMap<Int, Chapter>()
        var chapters = HashMap<Int, Chapter>()

        @SuppressLint("StaticFieldLeak")
        private lateinit var dialogGenres: DialogSelectionList

        @SuppressLint("StaticFieldLeak")
        private lateinit var dialogStatusCreatedBook: DialogSelectionList
    }

    @RequiresApi(Build.VERSION_CODES.P)
    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<CreateAndEditViewModel>()
        nameGenres =
            Array(Genres.values().size) { i -> getString(Genres.values()[i].nameG) }
        nameStatusCreatedBook =
            Array(StatusCreatedBooks.values().size) { i -> getString(StatusCreatedBooks.values()[i].nameSCB) }

        dialogGenres = DialogSelectionList(
            "Выберите жанр",
            nameGenres,
            getGenre()
        )

        setOnClickGenre()

        setStatusCreatedBook()
        dialogStatusCreatedBook = DialogSelectionList(
            "Выберите тип завершённости книги",
            nameStatusCreatedBook,
            getStatusCreateBook()
        )


        setOnClickStatusCreateBook()

        setClipToOutline()
        val isChangeChapter = arguments?.getBoolean(getString(R.string.is_change_chapter), false)!!
        println(isChangeChapter)
        if (!isChangeChapter) {
            book = Book(requireActivity().resources)
            deleteChapters = HashMap<Int, Chapter>()
            addedChapters = HashMap<Int, Chapter>()
            chapters = HashMap<Int, Chapter>()
            try {
                viewModel.idBook = arguments?.getInt(getString(R.string.id_book))!!
                changeBook(viewModel.idBook)
            } catch (e: Exception) {
                createBook()
            }
        } else {
            setCheckedSwitchDraft()
            viewModel.idBook = book.id
            if (viewModel.idBook == -1) {
                createBook()
            } else {
                changeBook(viewModel.idBook)
            }
        }


        val root: View = getRoot()
        val adapter = AdapterChaptersEdit(
            chapters,
            binding,
            dialogGenres,
            dialogStatusCreatedBook,
            parentFragmentManager
        )
        getListChapter().adapter = adapter
        setOnClickAddChapter(adapter)

        setOnClickAddCover()

//        binding.nameBook.set("answer")
        setOnClickCreate()

        viewModel.liveIsAdded.observe(viewLifecycleOwner) {
            if (it) {
                MainActivity.clearStack()
                val bundle = Bundle()
                bundle.putInt(
                    getString(R.string.id_book),
                    viewModel.addBookAll.idBook
                )
                MainActivity.navigate(R.id.nav_book, bundle)
            } else {
                Toast.makeText(
                    requireContext(),
                    "Не удалось добавить книгу",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        viewModel.liveIsChanged.observe(viewLifecycleOwner) {
            if (it) {
                MainActivity.clearStack()
                val bundle = Bundle()
                bundle.putInt(
                    getString(R.string.id_book),
                    viewModel.changeBookAll.idBook
                )
                MainActivity.navigate(R.id.nav_book, bundle)
            } else {
                Toast.makeText(
                    requireContext(),
                    "Не удалось изменить книгу",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        if (!isChangeChapter) {
            viewModel.liveBook.observe(viewLifecycleOwner) {
                book = it
                setInterfaceWithBook()
                viewModel.getListIdChapters.sendRequest(viewModel.idBook)
            }

            viewModel.liveChapters.observe(viewLifecycleOwner) {
                for (chapter in it) {
                    if (!chapters.containsKey(chapter.key)) {
                        chapters[chapter.key] = chapter.value
                    }
                }

                setInterfaceWithChapters()
            }
        }

        if (!isChangeChapter) {
            if (!isCreated) {
                update()
            }
        } else {
            println("updateInterface")
            setInterfaceWithBook()
            setInterfaceWithChapters()
        }

        requireActivity().onBackPressedDispatcher.addCallback(this /* lifecycle owner */) {
            MainActivity.navigateBack()
        }

        return root

    }

    private fun setCheckedSwitchDraft() {
        binding.switchDraft.isChecked = !book.isDraft
    }

    private fun setClipToOutline() {
        getCover().clipToOutline = true
    }

    private fun setStatusCreatedBook() {
        getStatusCreateBook().text = nameStatusCreatedBook[1]
    }

    private fun setOnClickStatusCreateBook() {
        getStatusCreateBook().setOnClickListener {
            dialogStatusCreatedBook.show(
                requireActivity().supportFragmentManager.beginTransaction(),
                "CreateAndEditFragment"
            )
        }
    }

    private fun getStatusCreateBook() = binding.statusCreateBook

    private fun getRoot() = binding.root

    private fun setOnClickCreate() {
        getCreate().setOnClickListener {
            if (binding.nameBook.text.toString().isNotEmpty()
                && binding.annotate.text.toString().isNotEmpty()
                && chapters.isNotEmpty()
                && !getGenre().text.equals(getString(R.string.genre_text_default))
            ) {
                if (isCreated) {
                    viewModel.addBookAll.sendRequest(
                        book.bitmap,
                        getCurrentBookClass(),
                        deleteChapters,
                        addedChapters,
                        changesChapters
                    )
                } else {
                    val bookClass = getCurrentBookClass()
                    bookClass.coverId = book.idCoverBook
                    viewModel.changeBookAll.sendRequest(
                        book.bitmap,
                        viewModel.idBook,
                        bookClass,
                        deleteChapters,
                        addedChapters,
                        changesChapters
                    )
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun setOnClickAddCover() {
        getCover().setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            resultLauncher.launch(intent)
        }
    }

    private fun setOnClickAddChapter(adapter: AdapterChaptersEdit) {
        getAddChapter().setOnClickListener {
            val builder: AlertDialog.Builder = AlertDialog.Builder(context)
            val input = EditText(context)
            edit = input
            val lp = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            input.layoutParams = lp
            builder.setView(input)

            val dialogClickListener =
                DialogInterface.OnClickListener { _, which ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> {
                            val nextIdAddedChapter = if (chapters.isEmpty()) {
                                1
                            } else {
                                Collections.max(chapters.keys) + 1
                            }
                            addedChapters[nextIdAddedChapter] =
                                Chapter(nextIdAddedChapter, input.text.toString())
                            chapters[nextIdAddedChapter] =
                                addedChapters[nextIdAddedChapter]!!
                            adapter.notifyDataSetChanged()
                        }
                        DialogInterface.BUTTON_NEGATIVE -> {}
                    }
                }

            builder.setMessage("Напишите название главы")
                .setPositiveButton(getString(R.string.dialog_add), dialogClickListener)
                .setNegativeButton("Отмена", dialogClickListener).show()
        }
    }

    private fun getAddChapter() = binding.addChapter

    private fun setOnClickGenre() {
        getGenre().setOnClickListener {
            dialogGenres.show(
                requireActivity().supportFragmentManager.beginTransaction(),
                "CreateAndEditFragment"
            )
        }
    }

    private fun getGenre() = binding.genre

    @SuppressLint("NotifyDataSetChanged")
    private fun setInterfaceWithChapters() {
        getListChapter().adapter!!.notifyDataSetChanged()
    }

    private fun getListChapter() = binding.listChapter

    private fun setInterfaceWithBook() {
        println(book)
        setDraft()
        setNameBookAndAnnotate()
        dialogGenres.setIdSelected(book.genre.id)
        dialogStatusCreatedBook.setIdSelected(book.completeness.id)
        getCover().setImageBitmap(book.bitmap)
    }

    private fun setNameBookAndAnnotate() {
        binding.nameBook.setText(book.name)
        binding.annotate.setText(book.annotate)
    }

    private fun createBook() {
        isCreated = true
        viewModel.idBook = -1
        MainActivity.toolbar.title = "Создание"
        getCreate().text = "Создать"
    }

    private fun changeBook(idBook: Int) {
        if (idBook > 0) {
            book.id = idBook
            isCreated = false
            MainActivity.toolbar.title = "Редактирование"
            getCreate().text = "Сохранить изменения"
        } else {
            createBook()
        }
    }

    private fun getCreate() = binding.create

    override fun update() {
        viewModel.getBookClass.sendRequest(idBook = viewModel.idBook)
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == RESULT_OK) {
                val selectedImageUri: Uri? = result.data?.data
                //binding.cover.setImageURI(selectedImageUri)
                //var bmp: Bitmap? = activity?.let {MediaStore.Images.Media.getBitmap(it.getContentResolver(), selectedImageUri)}
                val source = ImageDecoder.createSource(
                    requireActivity().contentResolver,
                    selectedImageUri!!
                )

                val bitmap = ImageDecoder.decodeBitmap(source)
                getCover().setImageBitmap(bitmap)
                book.bitmap = bitmap
            }
        }

    private fun getCover() = binding.cover

    private fun getCurrentBookClass(): BookClass {
        val bookClass = BookClass()
        bookClass.authorId = MainActivity.idUser
        bookClass.name = binding.nameBook.text.toString()
        bookClass.annotation = binding.annotate.text.toString()
        bookClass.genreId = dialogGenres.getIdSelected()
        bookClass.completenessId = dialogStatusCreatedBook.getIdSelected()
        setDraft()
        return bookClass
    }

    private fun setDraft() {
        book.isDraft = !binding.switchDraft.isChecked
    }

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?) {
        binding = FragmentCreateAndEditBinding.inflate(inflater, container, false)
    }

    override fun setSwipeRefreshLayout() {
        swipeRefreshLayout = binding.swipeRefreshLayout
    }

    override fun addFragmentToStack() {

    }

}