package com.shusharin.fanfictiontomorrow.requests.get_single

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequest
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genre
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListPresentBookInGenre
import retrofit2.Call

class PresentBookInGenreParams :
    MyRequestParams() {
    override fun toString(): String {
        return "PresentBookInGenreParams()"
    }
}

class GetPresentBookInGenre(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) :
    MyRequest<ListPresentBookInGenre, PresentBookInGenreParams, MutableList<Genre>>(
        startRefresh,
        stopRefresh
    ) {
    val liveGenres: MutableLiveData<MutableList<Genre>>
        get() = liveData

    fun sendRequest() {
        sendRequest(PresentBookInGenreParams())
    }
    override fun analysisOfResponseBody(
        responseBody: ListPresentBookInGenre,
        params: PresentBookInGenreParams,
    ) {
        setLiveData(
            if (responseBody.genres.size == 9) {
                responseBody.genres
            } else {
                MutableList(9) { index -> Genre(index + 1, false) }
            }
        )
    }

    override fun call(params: PresentBookInGenreParams): Call<ListPresentBookInGenre> {
        return service.getPresentBookInGenre()
    }

}