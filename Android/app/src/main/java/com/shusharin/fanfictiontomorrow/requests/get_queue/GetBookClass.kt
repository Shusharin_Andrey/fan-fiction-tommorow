package com.shusharin.fanfictiontomorrow.requests.get_queue

import android.content.res.Resources
import android.graphics.Bitmap
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.*
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Book
import com.shusharin.fanfictiontomorrow.requests.utils.classes.ReadBook
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusReadingBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import com.shusharin.fanfictiontomorrow.requests.utils.model.Person
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import retrofit2.Call

class BookClassParams(val idUser: Int, val idBook: Int) : MyRequestParams() {
    override fun toString(): String {
        return "BookClassParams(idUser=$idUser, idBook=$idBook)"
    }
}
class GetBookClass(
    override val owner: LifecycleOwner,
    override val api: MyApiServices,
    val resources: Resources,
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : MyRequest<BookClass, BookClassParams, Book>(startRefresh, stopRefresh),
    IGetLibraryBook,
    IGetLastChapter,
    IGetUser,
    IGetCover {



    private var book: Book = Book(resources)
    val liveBook: MutableLiveData<Book>
        get() = liveData

    fun sendRequest(idUser: Int = MainActivity.idUser, idBook: Int) {
        book = Book(resources)
        if (idBook != -1) {
            sendRequest(BookClassParams(idUser, idBook))
        }
    }

    override fun analysisOfResponseBody(
        responseBody: BookClass,
        params: BookClassParams,
    ) {
        if(isTest){
            book = Book(responseBody, resources)
            book.id = params.idBook
            book.idCoverBook = responseBody.coverId
            book.author = Person(responseBody.authorId)
            updateLiveData()
            return
        }

        if (responseBody.authorId == 0) {
            return
        }

        if (!responseBody.isDraft || responseBody.authorId == params.idUser) {
            book = Book(responseBody, resources)
            book.id = params.idBook

            if (responseBody.authorId != idUser && idUser != -1) {
                getLibraryBook(idUser, params.idBook)
            } else {
                book.statusReadingBook = StatusReadingBook.MY
            }
            book.idCoverBook = responseBody.coverId
            book.author = Person(responseBody.authorId)
            updateLiveData()
            getUser(responseBody.authorId)
            getCover(responseBody.coverId)
            getLastChapter(params.idUser, params.idBook)
        }
    }

    override fun call(params: BookClassParams): Call<BookClass> {
        return service.getBook(params.idUser, params.idBook)
    }

    override fun setAuthor(it: Person) {
        book.author = it
        updateLiveData()
    }

    private fun updateLiveData() {
        setLiveData(book)
    }

    override fun setCover(it: Bitmap) {
        book.bitmap = it
        updateLiveData()
    }

    override fun setLibraryBook(it: ReadBook) {
        book.statusReadingBook = it.statusReadingBook
        updateLiveData()
    }

    override fun setLastChapter(it: Int) {
        book.idLastChapter = it
        updateLiveData()
    }
}