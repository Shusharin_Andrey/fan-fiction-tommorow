package com.golovin.server.Services;

import com.golovin.server.DAO.LastBookDao;
import com.golovin.server.classes.ListBooksId;
import com.golovin.server.models.Books;
import com.golovin.server.models.LastBook;
import com.golovin.server.models.User;
import com.golovin.server.utils.MyCommon;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LastBookServiceTest {

    private final List<LastBook> lastBookList = new ArrayList<>();
    @InjectMocks
    protected MyCommon myCommon;
    @InjectMocks
    private LastBookService lastBookService;
    @Mock
    private LastBookDao lastBookDao;
    @Mock
    private Session session;
    @Mock
    private UserService userService;
    @Mock
    private BookService bookService;
    private LastBook lastBook1;
    private LastBook lastBook2;
    private User user1;
    private User user2;
    private Books book1;
    private Books book2;

    @BeforeEach
    void beforeEach() {
        Whitebox.setInternalState(LastBookService.class, "lastBookDao", lastBookDao);
        Whitebox.setInternalState(MyCommon.class, "lastBookService", lastBookService);
        Whitebox.setInternalState(MyCommon.class, "bookService", bookService);
        Whitebox.setInternalState(MyCommon.class, "userService", userService);

        user1 = new User();
        user1.setId(1);

        user2 = new User();
        user2.setId(2);

        book1 = new Books();
        book1.setId(1);

        book2 = new Books();
        book2.setId(2);

        lastBook1 = new LastBook(user1, book1, 1);
        lastBook2 = new LastBook(user1, book2, 2);

        lastBookList.add(lastBook1);
        lastBookList.add(lastBook2);

    }

    @Test
    void findAll() {
        when(lastBookDao.openCurrentSession()).thenReturn(session);
        when(lastBookDao.findAll(session)).thenReturn(lastBookList);
        List<LastBook> lastBookListTest = lastBookService.findAll();
        assertEquals(lastBookList.size(), lastBookListTest.size());
        assertEquals(lastBookList.get(0), lastBookListTest.get(0));
        assertEquals(lastBookList.get(1), lastBookListTest.get(1));
    }

    @Test
    void updateLastBooks() {
        when(lastBookDao.openCurrentSessionWithTransaction()).thenReturn(session);
        when(lastBookDao.openCurrentSession()).thenReturn(session);
        when(userService.getById(1)).thenReturn(user1);
        when(bookService.getById(1)).thenReturn(book1);
        when(lastBookDao.findUserLastBooks(session, 1)).thenReturn(lastBookList);
        when(lastBookDao.findBookWithMaxPriority(session, 1)).thenReturn(lastBook2);
        when(lastBookDao.findBookWithMinPriority(session, 1)).thenReturn(lastBook1);
        doNothing().when(lastBookDao).update(session, lastBook1);

        lastBookService.updateLastBooks(1, 1);

        List<LastBook> lastBookList = lastBookService.getLastBook(1);
        assertEquals(lastBookService.getMaxPriority(1), lastBookList.get(lastBookList.size() - 1).getPriority());

        when(lastBookDao.findUserLastBooks(session, 1)).thenReturn(new ArrayList<>());
        lastBookService.updateLastBooks(1, 1);

        List<LastBook> lastBooks = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            LastBook lastBook = new LastBook(new User(), new Books(), i + 4);
            lastBooks.add(lastBook);
        }
        when(lastBookDao.findUserLastBooks(session, 1)).thenReturn(lastBooks);
        lastBookService.updateLastBooks(1, 1);
        for (LastBook lastBook : lastBooks) {
            System.out.println(lastBook.getPriority());
        }
    }

    @Test
    void getLastBook() {
        when(lastBookDao.openCurrentSession()).thenReturn(session);
        when(lastBookDao.findUserLastBooks(session, 1)).thenReturn(lastBookList);
        List<LastBook> lastBookListTest = lastBookService.getLastBook(1);
        assertEquals(1, lastBookListTest.get(0).getBook().getId());
        assertEquals(1, lastBookListTest.get(0).getPriority());
        assertEquals(1, lastBookListTest.get(0).getUser().getId());

        assertEquals(2, lastBookListTest.get(1).getBook().getId());
        assertEquals(2, lastBookListTest.get(1).getPriority());
        assertEquals(1, lastBookListTest.get(1).getUser().getId());
    }

    @Test
    void getBookWithMinPriority() {
        when(lastBookDao.openCurrentSession()).thenReturn(session);
        when(lastBookDao.findBookWithMinPriority(session, 1)).thenReturn(lastBook1);
        LastBook lastBook = lastBookService.getBookWithMinPriority(1);
        assertEquals(1, lastBook.getPriority());
        assertEquals(1, lastBook.getUser().getId());
        assertEquals(1, lastBook.getBook().getId());
    }

    @Test
    void getMinPriority() {
        when(lastBookDao.openCurrentSession()).thenReturn(session);
        when(lastBookDao.findBookWithMinPriority(session, 1)).thenReturn(lastBook1);
        int minPriority = lastBookService.getMinPriority(1);
        assertEquals(1, minPriority);

        when(lastBookDao.findBookWithMinPriority(session, 31)).thenReturn(null);
        minPriority = lastBookService.getMinPriority(31);
        assertEquals(-1, minPriority);

    }

    @Test
    void getMaxPriority() {
        when(lastBookDao.openCurrentSession()).thenReturn(session);
        when(lastBookDao.findBookWithMaxPriority(session, 1)).thenReturn(lastBook2);
        int minPriority = lastBookService.getMaxPriority(1);
        assertEquals(2, minPriority);

        when(lastBookDao.findBookWithMaxPriority(session, 31)).thenReturn(null);
        minPriority = lastBookService.getMaxPriority(31);
        assertEquals(-1, minPriority);
    }


    @Test
    void getLastBooks() {
        when(lastBookDao.openCurrentSession()).thenReturn(session);
        when(lastBookDao.findUserLastBooks(session, 1)).thenReturn(lastBookList);
        ListBooksId listBooksId = lastBookService.getLastBooks(1);
        listBooksId.getBooksId().sort(Integer::compareTo);
        assertEquals(1, listBooksId.getBooksId().get(0));
        assertEquals(2, listBooksId.getBooksId().get(1));

        when(lastBookDao.findUserLastBooks(session, 2)).thenReturn(new ArrayList<>());
        listBooksId = lastBookService.getLastBooks(2);
        assertEquals(listBooksId.getBooksId().size(), 0);
    }
}