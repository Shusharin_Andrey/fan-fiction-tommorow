package com.golovin.server.Services;

import com.golovin.server.DAO.CoverDao;
import com.golovin.server.models.Cover;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CoverServiceTest {

    private final List<Cover> coverList = new ArrayList<>();
    @Mock
    private CoverDao coverDao;
    @Mock
    private Session session;
    @InjectMocks
    private CoverService coverService;
    private Cover cover1;
    private Cover cover2;

    @BeforeEach
    void beforeEach() {
        Whitebox.setInternalState(CoverService.class, "coverDao", coverDao);

        coverService = new CoverService();

        cover1 = new Cover();
        cover1.setDate("E:/a/a");
        cover1.setId(1);

        cover2 = new Cover();
        cover2.setDate("E:/b/b");
        cover2.setId(2);

        coverList.add(cover1);
        coverList.add(cover2);
    }

    @Test
    void getById() {
        when(coverDao.openCurrentSession()).thenReturn(session);
        when(coverDao.findById(session, 1)).thenReturn(cover1);
        Cover coverTest = coverService.getById(1);
        assertEquals(cover1.getDate(), coverTest.getDate());
        assertEquals(cover1.getId(), coverTest.getId());

        when(coverDao.findById(session, -2)).thenReturn(null);
        coverTest = coverService.getById(-2);
        assertNull(coverTest);
    }

    @Test
    void update() {
        Cover coverTest = new Cover();
        coverTest.setDate("aaa");
        coverTest.setId(1);

        when(coverDao.openCurrentSessionWithTransaction()).thenReturn(session);
        ArgumentCaptor<Cover> valueCapture = ArgumentCaptor.forClass(Cover.class);
        doNothing().when(coverDao).update(any(Session.class), valueCapture.capture());
        coverService.update(cover1);
        assertEquals(1, valueCapture.getValue().getId());
        assertEquals("E:/a/a", valueCapture.getValue().getDate());
        coverService.update(coverTest);
        assertEquals(1, valueCapture.getValue().getId());
        assertEquals(coverTest.getDate(), valueCapture.getValue().getDate());


    }

    @Test
    void addCover() {
        when(coverDao.openCurrentSessionWithTransaction()).thenReturn(session);
        ArgumentCaptor<Cover> valueCapture = ArgumentCaptor.forClass(Cover.class);
        doNothing().when(coverDao).persist(any(Session.class), valueCapture.capture());
        coverService.addCover(cover1.getDate());
        assertEquals("E:/a/a", valueCapture.getValue().getDate());

    }

    @Test
    void findAll() {
        when(coverDao.openCurrentSession()).thenReturn(session);
        when(coverDao.findAll(session)).thenReturn(coverList);
        List<Cover> covers = coverService.findAll();
        assertNotNull(covers);
        assertEquals(cover1.getDate(), covers.get(0).getDate());
        assertEquals(cover2.getDate(), covers.get(1).getDate());
    }
}