package com.golovin.server.classes;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LibraryBook {
    @JsonProperty
    private String name;

    @JsonProperty
    private int authorId;

    @JsonProperty
    private int coverId;

    @JsonProperty
    private int statusId;

    @JsonProperty
    private boolean isDraft;

    @JsonProperty
    private int genreId;

    @JsonProperty
    private int completenessId;

    public LibraryBook() {
    }

    public LibraryBook(String name, int authorId, int coverId, int statusId, boolean isDraft, int genreId, int completenessId) {
        this.name = name;
        this.authorId = authorId;
        this.coverId = coverId;
        this.statusId = statusId;
        this.isDraft = isDraft;
        this.genreId = genreId;
        this.completenessId = completenessId;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public int getCompletenessId() {
        return completenessId;
    }

    public void setCompletenessId(int completenessId) {
        this.completenessId = completenessId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public int getCoverId() {
        return coverId;
    }

    public void setCoverId(int coverId) {
        this.coverId = coverId;
    }

    public boolean getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(boolean draft) {
        isDraft = draft;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

}
