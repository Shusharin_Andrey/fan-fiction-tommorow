package com.golovin.server.classes;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.ArrayList;

public class ListChapterInBook {
    @JsonDeserialize(as = ArrayList.class, contentAs = ChaptersInBook.class)
    ArrayList<ChaptersInBook> chapters;

    public ListChapterInBook() {
        chapters = new ArrayList<>();
    }

    public ArrayList<ChaptersInBook> getChapters() {
        return chapters;
    }

    public void setChapters(ArrayList<ChaptersInBook> chapters) {
        this.chapters = chapters;
    }

}
