package com.golovin.server.utils;

import com.golovin.server.models.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    private HibernateSessionFactoryUtil() {
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
                configuration.addAnnotatedClass(Genres.class);
                configuration.addAnnotatedClass(Books.class);
                configuration.addAnnotatedClass(Chapters.class);
                configuration.addAnnotatedClass(User.class);
                configuration.addAnnotatedClass(Completeness.class);
                configuration.addAnnotatedClass(Cover.class);
                configuration.addAnnotatedClass(Status.class);
                configuration.addAnnotatedClass(Library.class);
                configuration.addAnnotatedClass(LastBook.class);

                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().
                        applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());

            } catch (Exception e) {
                System.out.println("Исключение!" + e);
            }
        }
        return sessionFactory;
    }
}