package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface ChaptersDaoInterface<T, Id extends Serializable> {
    void persist(Session session, T entity);

    void update(Session session, T entity);

    T findById(Session session, Id id);

    void delete(Session session, T entity);

    List<T> findAll(Session session);

    T findChapterOfBook(Session session, Id id);

    T findLastChapter(Session session, Id id);

    T findFirstChapter(Session session, Id id);

}
