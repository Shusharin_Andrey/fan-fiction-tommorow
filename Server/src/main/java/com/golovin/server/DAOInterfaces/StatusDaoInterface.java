package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;

public interface StatusDaoInterface<T, Id extends Serializable> {
    T findById(Session session, Id id);
}