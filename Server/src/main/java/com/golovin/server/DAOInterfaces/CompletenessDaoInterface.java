package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;

public interface CompletenessDaoInterface<T, Id extends Serializable> {
    T findById(Session session, Id id);

}
