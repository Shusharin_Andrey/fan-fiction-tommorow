package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface LibraryDaoInterface<T, Id extends Serializable> {
    void persist(Session session, T entity);

    void delete(Session session, T entity);

    List<T> getBooksWithStatus(Session session, Id id1, Id id2);

    List<T> getAllBooks(Session session, Id id);

    T getUserBookStatus(Session session, Id id1, Id id2);

    T addBookInLibrary(Session session, Id id1, Id id2, Id id3);

    List<T> searchBooks(Session session, String text, Id id1, Id type, Id id2);
}
