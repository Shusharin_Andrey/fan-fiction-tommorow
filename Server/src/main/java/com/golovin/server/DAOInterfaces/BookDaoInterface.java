package com.golovin.server.DAOInterfaces;

import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

public interface BookDaoInterface<T, Id extends Serializable> {

    void persist(Session session, T entity);

    void update(Session session, T entity);

    void delete(Session session, T entity);

    T findById(Session session, Id id);

    List<T> findAll(Session session);

    boolean isGenrePresent(Session session, Id id);

    T getUserBook(Session session, Id id1, Id id2);

    List<T> getAuthor(Session session, Id id);

    List<T> getGenre(Session session, Id id);

    List<T> getBookInSection(Session session, Id id);

    List<T> searchBooks(Session session, String text, Id id1, Id type);
}