package com.golovin.server.Services;

import com.golovin.server.DAO.GenresDao;
import com.golovin.server.models.Genres;
import org.hibernate.Session;

import java.util.List;

public class GenresService {

    private static final GenresDao genresDao = new GenresDao();

    public GenresService() {
    }


    public Genres getById(int id) {
        Session session = genresDao.openCurrentSession();
        Genres genre = genresDao.findById(session, id);
        genresDao.closeCurrentSession(session);
        return genre;
    }

    public List<Genres> findAll() {
        Session session = genresDao.openCurrentSession();
        List<Genres> genres = genresDao.findAll(session);
        genresDao.closeCurrentSession(session);
        return genres;
    }

}