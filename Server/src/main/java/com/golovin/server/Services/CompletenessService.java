package com.golovin.server.Services;

import com.golovin.server.DAO.CompletenessDao;
import com.golovin.server.models.Completeness;
import org.hibernate.Session;

public class CompletenessService {

    private static final CompletenessDao completenessDao = new CompletenessDao();

    public CompletenessService() {
    }

    public Completeness getById(int id) {
        Session session = completenessDao.openCurrentSession();
        Completeness completeness = completenessDao.findById(session, id);
        completenessDao.closeCurrentSession(session);
        return completeness;
    }

}
