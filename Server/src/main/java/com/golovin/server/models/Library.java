package com.golovin.server.models;


import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "библиотека")
public class Library {
    @EmbeddedId
    private LibraryPK pk;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    @JoinColumn(name = "книга", insertable = false, updatable = false)
    private Books book;
    @ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
    @JsonBackReference
    @JoinColumn(name = "пользователь", insertable = false, updatable = false)
    private User user;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    @JoinColumn(name = "статус", insertable = false, updatable = false)
    private Status status;

    public Library() {
    }

    public Library(LibraryPK pk, Books book, Status status, User user) {
        this.book = book;
        this.status = status;
        this.user = user;
        this.setPk(new LibraryPK(book.getId(), status.getId(), user.getId()));

    }

}
