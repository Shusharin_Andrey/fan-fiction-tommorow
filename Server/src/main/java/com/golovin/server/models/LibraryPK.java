package com.golovin.server.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
public class LibraryPK implements Serializable {

    @Column(name = "книга")
    private int bookId;

    @Column(name = "статус")
    private int statusId;

    @Column(name = "пользователь")
    private int userId;

    public LibraryPK(int book, int status, int user) {
        this.bookId = book;
        this.statusId = status;
        this.userId = user;
    }

    public LibraryPK() {
    }

}
