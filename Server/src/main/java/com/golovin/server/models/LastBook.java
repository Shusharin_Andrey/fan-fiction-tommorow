package com.golovin.server.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Table(name = "последние_книги")
public class LastBook {

    @EmbeddedId
    private LastBookPK pk;
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    @JoinColumn(name = "пользователь", insertable = false, updatable = false)
    private User user;
    @ManyToOne(fetch = FetchType.EAGER)
    @JsonBackReference
    @JoinColumn(name = "книга", insertable = false, updatable = false)
    private Books book;
    @Column(name = "приоритет")
    private int priority;

    public LastBook() {
    }


    public LastBook(User user, Books book, int number) {
        this.pk = new LastBookPK(book.getId(), user.getId());
        this.user = user;
        this.book = book;
        this.priority = number;
    }

}
