package com.golovin.server.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Data
@Table(name = "пользователь")
@EqualsAndHashCode
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "имя")
    private String name;

    @Column(name = "фамилия")
    private String surname;

    @OneToMany(mappedBy = "author", fetch = FetchType.EAGER)
    private List<Books> booksList;

    @ManyToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "последняя_глава",
            joinColumns = @JoinColumn(name = "пользователь"),
            inverseJoinColumns = @JoinColumn(name = "глава"))
    private List<Chapters> userChaptersList;
    private String google_auth;

    public User(String name, String surname, String google_auth) {
        this.name = name;
        this.surname = surname;
        booksList = new ArrayList<>();
        userChaptersList = new ArrayList<>();
        this.google_auth = google_auth;
    }

    public User() {
    }

    public void addChapter(Chapters chapter) {
        userChaptersList.add(chapter);
        chapter.getUserList().add(this);
    }

    public void removeChapter(Chapters chapter) {
        userChaptersList.remove(chapter);
        chapter.getUserList().remove(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return name.equals(user.name) && surname.equals(user.surname) && google_auth.equals(user.google_auth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, google_auth);
    }

}
