package com.golovin.server.DAO;

import com.golovin.server.DAOInterfaces.LastBookDaoInterface;
import com.golovin.server.models.LastBook;
import com.golovin.server.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class LastBookDao implements LastBookDaoInterface<LastBook, Integer> {

    private Session currentSession;

    private Transaction currentTransaction;

    public LastBookDao() {
    }

    public Session openCurrentSession() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(Session session) {
        session.close();
    }

    public void closeCurrentSessionWithTransaction(Session session) {
        session.getTransaction().commit();
        session.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }


    @Override
    public void persist(Session session, LastBook lastBook) {
        session.save(lastBook);
    }

    @Override
    public void update(Session session, LastBook lastBook) {
        session.update(lastBook);
    }

    @Override
    public void delete(Session session, LastBook lastBook) {
        session.delete(lastBook);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<LastBook> findAll(Session session) {
        return (List<LastBook>) session.createQuery("FROM LastBook").list();
    }

    public List<LastBook> findUserLastBooks(Session session, Integer userId) {
        String queryString = "from LastBook where user.id = :userId";
        Query query = session.createQuery(queryString);
        query.setParameter("userId", userId);
        if (query.list().isEmpty()) {
            return new ArrayList<>();
        }
        List<LastBook> lastBooks = (ArrayList<LastBook>) query.list();


        return lastBooks;

    }

    @Override
    public LastBook findBookWithMaxPriority(Session session, Integer userId) {
        String queryString = "from LastBook where user.id = :userId order by priority asc";
        Query query = session.createQuery(queryString);
        query.setParameter("userId", userId);
        if (query.list().isEmpty()) {
            return null;
        }
        List<LastBook> lastBooks = (ArrayList<LastBook>) query.list();
        return lastBooks.get(lastBooks.size() - 1);
    }

    @Override
    public LastBook findBookWithMinPriority(Session session, Integer userId) {
        String queryString = "from LastBook where user.id = :userId order by priority desc";
        Query query = session.createQuery(queryString);
        query.setParameter("userId", userId);
        if (query.list().isEmpty()) {
            return null;
        }
        List<LastBook> lastBooks = (ArrayList<LastBook>) query.list();
        return lastBooks.get(lastBooks.size() - 1);
    }

}
