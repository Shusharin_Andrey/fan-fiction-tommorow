package com.golovin.server.DAO;

import com.golovin.server.DAOInterfaces.ChaptersDaoInterface;
import com.golovin.server.models.Chapters;
import com.golovin.server.utils.HibernateSessionFactoryUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class ChaptersDao implements ChaptersDaoInterface<Chapters, Integer> {
    private Session currentSession;

    private Transaction currentTransaction;

    public ChaptersDao() {
    }

    public Session openCurrentSession() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(Session session) {
        session.close();
    }

    public void closeCurrentSessionWithTransaction(Session session) {
        session.getTransaction().commit();
        session.close();
    }


    public Session getCurrentSession() {
        return currentSession;
    }

    @Override
    public void persist(Session session, Chapters chapter) {
        session.saveOrUpdate(chapter);
    }

    @Override
    public void update(Session session, Chapters chapter) {
        session.update(chapter);
    }

    @Override
    public Chapters findById(Session session, Integer id) {
        Chapters chapter = session.get(Chapters.class, id);
        return chapter;
    }

    @Override
    public void delete(Session session, Chapters chapter) {
        session.delete(chapter);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Chapters> findAll(Session session) {
        return (List<Chapters>) session.createQuery("FROM Chapters").list();
    }

    @Override
    public Chapters findChapterOfBook(Session session, Integer chapterId) {
        String queryString = "from Chapters where id = :chapterId";
        Query query = session.createQuery(queryString);
        query.setParameter("chapterId", chapterId);
        if (query.list().isEmpty()) {
            return null;
        }
        Chapters chapters = (Chapters) query.getSingleResult();

        return chapters;
    }

    @Override
    public Chapters findLastChapter(Session session, Integer bookId) {
        String queryString = "from Chapters where book.id = :bookId ORDER BY number asc";
        Query query = session.createQuery(queryString);
        query.setParameter("bookId", bookId);
        if (query.list().isEmpty()) {
            return null;
        }
        List<Chapters> chapters = query.list();
        Chapters chapter = chapters.get(chapters.size() - 1);
        return chapter;
    }

    @Override
    public Chapters findFirstChapter(Session session, Integer bookId) {
        String queryString = "from Chapters where book.id = :bookId ORDER BY number desc";
        Query query = session.createQuery(queryString);
        query.setParameter("bookId", bookId);
        if (query.list().isEmpty()) {
            return null;
        }
        List<Chapters> chapters = query.list();
        Chapters chapter = chapters.get(chapters.size() - 1);
        return chapter;
    }


}
